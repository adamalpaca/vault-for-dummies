storage "file" {
  path    = "/c/Users/adam.true/Documents/vault"
}

listener "tcp" {
 address     = "127.0.0.1:8200"
 tls_disable = 1
}

api_addr = "http://127.0.0.1:82000"