# Using vault

This document will delve into the use of the software "Vault" used as a secrets engine. The tutorials can be found in the first part but for a step-by-step hands-on go to "**Set Up Your Server**".

## Tools 

* [**vault**](https://www.hashicorp.com/products/vault/).
* [**jq**](https://stedolan.github.io/jq/download/): an inline JSON generator
* [**consul**](https://www.consul.io/downloads.html): a tool for encrypted data storage and communication. Generally used as backend for ``vault`` (see **Deployment**).

**Don't forget to add these executables to the path if you are using [Windows!](https://duckduckgo.com/?q=kill+me+now+gif&t=ffab&atb=v179-1&iax=images&ia=images&iai=https%3A%2F%2Fmedia.giphy.com%2Fmedia%2FIIozzNpFTvIGc%2Fgiphy.gif)**

---

## Tutorial Steps

Extensive documentation is provided to start using Vault. This can be found [here](https://learn.hashicorp.com/vault).

### Steps to test with a dev server

Open an instance of your console and execute the command:

```
$ vault server -dev
```

You will see something along these lines:

```
Unseal Key: HzCtx2dwLopg0UFhEJ64JUfjsZYFOH4wk5Z1xIQZm4Y=
Root Token: s.yAGf1LFBVbptYNTIwXPvLzrK
```

Keep these safe somewhere and keep this console running (it runs in the foreground unfortunately [😞](https://www.lucas-bonvin.com/)). 

You will also find this on Windows (sorry I'm a Windows boii) but there's something similar on Linux: 

```
PowerShell:
    $env:VAULT_ADDR="http://127.0.0.1:820
cmd.exe:
    set VAULT_ADDR=http://127.0.0.1:8200
```

Open a new console and set the ``VAULT_ADDR`` variable as suggested. In this new instance of the console you can do the following tutos: 

* [Starting a Dev Server](https://learn.hashicorp.com/vault/getting-started/dev-server)
* [Your First Secret](https://learn.hashicorp.com/vault/getting-started/first-secret)
* [Using Vault with Files](https://www.vaultproject.io/docs/commands/index.html), under the section ``Files``
* [Authentication](https://learn.hashicorp.com/vault/getting-started/authentication) - especially interesting is the part on Github authentication because you *should not* use the method ``vault token create``
* [Policies and Authorization](https://learn.hashicorp.com/vault/getting-started/policies)

### Deployment

* [Configuration and Deployment](https://learn.hashicorp.com/vault/getting-started/deploy)
    - In this example we must first launch another console, this time running a ``consul`` server.
    - When executing the operator init command, you can come accross this error:
    ```
    $ vault operator init
    Error initializing: Put https://127.0.0.1:8200/v1/sys/init: http: server gave HTTP response to HTTPS client
    ```
    The simple solution to this is to set the environment variable ``VAULT_ADDR`` to ``http://127.0.0.1:8200``. The reason for this is they suggest creating a server config file where ``TLS`` is disabled ... When *really* deploying, however, this will not be the case.
    - When executing this command ``vault`` will provide you with a series of unseal keys. Normally you would save these keys in seperate places (p.ex on the USB drives of five members of the executive board) and then you would need to use at least three of them together in order to unlock the vault [(see Shamir's Secret Sharing)](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing). The power of ``vault`` is having a stately unlocking mechanism, meaning the process can be done on multiple PCs and the keys *never need to be together*.
* [Deployment strategies using Dockers](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
* [Using the APIs](https://learn.hashicorp.com/vault/getting-started/apis). Vault provides a HTTP API

---
---

## Set Up Your Server - Hands-On

The steps you need to take in order to set up your server are the following.

**(Launch the ``consul`` server if using ``consul``)**

Open a new terminal and launch the command:

```
$ consul agent -dev
```

---

**Launch your ``vault`` server**

* Create a [config](https://www.vaultproject.io/docs/configuration/index.html) file ``config.hcl`` containing this for ``consul``:

```
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault/"
}

listener "tcp" {
 address     = "127.0.0.1:8200"
 tls_disable = 1
}
```

Or to use ``vault`` with the file system:

```
storage "file" {
  path    = "mnt/vault/data"
}

listener "tcp" {
 address     = "127.0.0.1:8200"
 tls_disable = 1
}
```

* Open a new cmd and launch the ``vault`` server:

```
$ vault server -config=config.hcl
```

---

**Start Server**

Open a new terminal and initialise the vault:

```
$ vault operator init -key-shares=5 -key-threshold=3
```

The values of ``key-shares`` and ``key-threshold`` are up to you.
The value of shares will be the amount of keys distributed.
The value of the threshold is the amount of keys needed to unseal the vault.

See [Shamir's Secret Sharing](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing) for more info. 

It will provide you with the following:

```
Unseal Key 1: sNxb+VIgqTzukErgFrO08oigyB7Ca0GsEA9/NNpY4c62
Unseal Key 2: EkfVwgrPnH/Ar/BgshHD4y9CjNdFxXvJIyhLs4ENoBFX
Unseal Key 3: h4FzIcmxbCu1bXd2xTWG9lOyFwFAOq8JULzUJEr6iLpd
Unseal Key 4: 5VVHSBLqG7APLj7OWEgJyP0lfp5D7djuXweYiV2TojR6
Unseal Key 5: ukN9B16MTPSN6az6r9wJo5voJ7Bh0iQ1W2+IacgsRppv

Initial Root Token: s.r2JjhdnwUtInJTrGAWklbzj8
```

**You can only initialise the vault once at the start, after this it is initialised and cannot be modified so keep these values safe.**

---

**Unseal Server**

To unseal the server we must execute this command ``key-threshold`` amount of times:

```
$ vault operator unseal
```

It will request one of the keys from the earlier steps and each time we must give it a different one or it won't work. We know it has worked when the value ``sealed`` is false:

```
Unseal Key (will be hidden):
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    5
Threshold       3
Version         1.2.3
Cluster Name    vault-cluster-0e5adb6b
Cluster ID      e8c92ca2-7fa4-0a79-8b37-068a00517a1d
HA Enabled      false
```

---

**Log in to Server**

Now we are ready to log in to the server as root (with the root token from before):

```
$ vault login s.r2JjhdnwUtInJTrGAWklbzj8
```

---

**Policy Creation**

Here we will create policies for the secrets. Create a file ``my_policy.hcl``:

```
path "secret/*" {
  capabilities = ["create"]
}
path "secret/foo" {
  capabilities = ["read"]
} 
```

This means we can create any secret in ``secret/`` but can only read ``secret/foo``. 

```
$ vault policy write my-policy ./my-policy.hcl
Success! Uploaded policy: my-policy
```

---

**Enabling the secrets engine**

Enable the secrets engine for the path ``secret/``. Unless you want to create a different path.

```
$ vault secrets enable -path=secret/ kv
```

Now we can test that it works:

```
$ vault kv put secret/foo key=1245
Success! Data written to: secret/foo

$ vault kv put secret/world hello=guys
Success! Data written to: secret/world

$ vault kv get secret/foo
=== Data ===
Key    Value
---    -----
key    1245

$ vault kv get secret/world
==== Data ====
Key      Value
---      -----
hello    guys
```

---

**Setting up Github Authentication**

Set up the authentication method with the following:

```
$ vault auth enable -path=github github
Success! Enabled github auth method at: github/
```

Now we want to add organisations and teams to this. We will use the organization to authicate users and we will map policies to teams.

```
$ vault write auth/github/config organization=adams-team 
Success! Data written to: auth/github/config
```

(Yes I named my organisation adams-team and not adams-organization sorry 🤷)

```
$ vault write auth/github/map/teams/my-team value=default,my-policy
Success! Data written to: auth/github/map/teams/my-team
```

This means any member of the team "my-team" in the organisation "adams-team" will be appointed the policy "my-policy". This policy was the one we applied earlier. 

Now we need to generate a Github User Token:
* Settings -> Developer Settings -> Personal Acces Tokens
* Create a token with ``repo`` rights (others *might* work but I'm unsure. I do know that ``repo`` tokens will work)
* Warning ⚠️ - Github will only reveal this token once so store it in a safe place (like a password manager)

Let's log in with Github and test the policy:

```
$ vault login -method=github
GitHub Personal Access Token (will be hidden):
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.oKVIm1zCpJf48NB2zI23jQ5C
token_accessor         Qz1FHFBybDBugVjIMVx8BVqK
token_duration         768h
token_renewable        true
token_policies         ["default" "my-policy"]
identity_policies      []
policies               ["default" "my-policy"]
token_meta_org         adams-team
token_meta_username    Adam-True

$ 
```


Ok now we are logged in, we can see that the policy is applied to this user. Let's test it out:

```
$ vault read secret/foo
Key                 Value
---                 -----
refresh_interval    768h
key                 1245

$ vault read secret/hello
Error reading secret/hello: Error making API request.

URL: GET http://127.0.0.1:8200/v1/secret/hello
Code: 403. Errors:

* 1 error occurred:
        * permission denied


$ vault write secret/usersecret password=isasecret
Success! Data written to: secret/usersecret

$ vault read secret/usersecret
Error reading secret/usersecret: Error making API request.

URL: GET http://127.0.0.1:8200/v1/secret/usersecret
Code: 403. Errors:

* 1 error occurred:
        * permission denied
```

Great! You can see that we can only read ``secret/foo``, and we can write ``secret/*``.

---

**Using the API**

Sorry but this part is quite a bother to do on Windows so I'll leave it up to you for the moment. In Linux the ``curl`` operation allows you to use the API quite freely.